
fetch('https://jsonplaceholder.typicode.com/posts').then((response) => response.json()).then((data) => showPosts(data));

document.querySelector("#form-add-post").addEventListener('submit', (e) => {

	// Prevents the default behavior of an event. To prevent the page from reloading (default behavior of submit)

	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts', {

		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value, 
			body: document.querySelector('#txt-body').value, 
			userId: 1
		}),
		headers: {
			'Content-Type': 'application/json; charset=UTF-8'
		}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert("Successfully Added!");

		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;

	});
});

// RETRIEVE Post Data

const showPosts = (posts) => {

	// variable that will contain all the posts
	let postEntries = "";

	// Looping through array items
	posts.forEach((post) => {

		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>`
	});

	document.querySelector('#div-post-entries').innerHTML = postEntries;
};

// EDIT Post (Edit Button)

const editPost = (id) => {

	// The function first uses the querySelector() method t oget the element with the id "#post-title-${id}" and "post-body-${id}" and assign its innerHTML property to the title variable with the same body.

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

	// removeAttribute() - removes the attribute with the specified name from the element
	document.querySelector('#btn-submit-update').removeAttribute('disabled');
};

// UPDATE Post (Update Button)

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/100', {

		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		headers: {
			'Content-Type': 'application/json; charset=UTF-8'
		}
	})
	.then((response) => response.json())
	.then((data) => {

		console.log(data);
		alert("Successfully Updated!");

		// Reset the edit post form from input fields
		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;

		// setAttribute() - adds/sets an attribute to an HTML element
		document.querySelector('#btn-submit-update').setAttribute('disabled', true);
	});
});

// DELETE Post (Delete Button) 

const deletePost = (id) => {

	fetch('https://jsonplaceholder.typicode.com/posts/1', {

		method: 'DELETE',
		headers: {
			'Content-Type': 'application/json; charset=UTF-8'
		}
	}).then((response) => response.json())
	.then((data) =>
	{
		let title = document.querySelector(`#post-title-${id}`);
		let body = document.querySelector(`#post-body-${id}`);
		
		document.querySelector(`#post-${id}`).innerHTML = null;
	});
};